const getNews = (callback) => {
  const query = `news(where: {enabled: true}, sort: "date:desc"){title, subtitle, selectType, cardColor, videourl, date, thumbnail { url }, enabled}`

  $.when(
    $.ajax({
      url: `/api/strapi?q=${query}`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.error(`There was an error in the request: ${error}`)
      }
    })
  ).then(function (data) {
    callback(data)
  })
}
