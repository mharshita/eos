const express = require('express')
const router = express.Router()
const nodemailer = require('nodemailer')

router.post('/', (req, res) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.SMTP_EMAIL,
      pass: process.env.SMTP_TOKEN
    }
  })

  const mailOptions = {
    from: 'uxuisuseteam@gmail.com',
    to: process.env.SMTP_RECIPIENT,
    subject:
      req.body.type === 'story'
        ? 'New success story message!'
        : 'New feedback message',
    html:
      req.body.type === 'story'
        ? `<p>${req.body.message}</p>`
        : `<p>${req.body.message}</p> ${
            req.body.img !== null ? '<h5>Screenshot attached:</h5>' : ''
          } `
  }

  if (req.body.img) mailOptions.attachments = [{ path: req.body.img }]

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error)
      res.status(500).send('Something went wrong =/')
    } else {
      res.status(200).send('Message was sent!')
      console.log(`Email sent: ${info.response}`)
    }
  })
})

module.exports = router
