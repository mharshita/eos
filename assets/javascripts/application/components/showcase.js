let $eleDisplayTemplate
$(function () {
  Prism.highlightAll() // eslint-disable-line no-undef
  const name = $('.js-element-id').attr('name') // eslint-disable-line no-unused-vars
  // Prepare li to show component showcase radio buttons
  $eleDisplayTemplate = $('.js-showcase-current').clone(true)
  $('.js-showcase-current').remove()
  if (name !== undefined) {
    getComponentType(name)
  }
})

const renderComponentShowcase = (componentShowcases) => {
  const componentCollection = componentShowcases.map((ele) => ele.states)
  for (let i = 0; i < componentCollection[0].length; i++) {
    const compShowcase = componentCollection[0][i].status
    const newEleDisplay = $eleDisplayTemplate.clone(true)

    // Make first letter uppercase and add space between words
    const labelText =
      compShowcase.substr(0, 1).toUpperCase() +
      compShowcase.substr(1).split('-').join(' ')

    // Add id value to input field
    $(newEleDisplay)
      .find('.js-element-id')
      .attr({ id: `${compShowcase}-showcase`, value: `${compShowcase}` })
    $(newEleDisplay)
      .find('.js-element-label')
      .attr('for', `${compShowcase}-showcase`)
      .text(`${labelText}`)
    $('.js-showcase-list').append(newEleDisplay)
  }

  // Add checked attibute to default showcase on page load
  $('#default-showcase').prop('checked', true)

  // Add default text to How to use and code container on page load
  updateComponentShowcase(componentCollection[0], 'default')

  // Change text in How to use and code container on click
  $('.js-showcase-current input').click(function () {
    const component = $(this).attr('value')

    // Update components info
    updateComponentShowcase(componentCollection[0], component)
  })
}

const updateComponentShowcase = (componentCollection, component) => {
  const componentShowcaseData = componentCollection.filter(
    (ele) => ele.status === `${component}`
  )

  if (componentShowcaseData[0].how_to_use !== '') {
    $('.js-how-to-use').html(componentShowcaseData[0].how_to_use)
    $('.js-showcase-how-to').show()
    $('.js-how-to-use').show()
  } else {
    $('.js-showcase-how-to').hide()
    $('.js-how-to-use').hide()
  }

  if (componentShowcaseData[0].alert !== undefined) {
    let alertCode = ''
    for (let i = 0; i < componentShowcaseData[0].alert.length; i++) {
      alertCode += `${componentShowcaseData[0].alert[i]}
  `
    }
    $('.js-snippet-alert').html(alertCode)
    $('.js-snippet-alert').show()
  } else {
    $('.js-snippet-alert').hide()
  }

  let newCode = ''
  for (let i = 0; i < componentShowcaseData[0].code.length; i++) {
    newCode += `${componentShowcaseData[0].code[i]}
`
  }
  $('.js-example-inner-box').html(newCode)
  $('.js-snippet-code code').text(newCode)
  Prism.highlightAll() // eslint-disable-line no-undef
  $('*[data-toggle~="tooltip"]').tooltip('update')
}

function getComponentType (name) {
  getComponentService(name, (result) => {
    const componentShowcases = result

    renderComponentShowcase(componentShowcases)
  })
}
