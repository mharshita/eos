describe('Tooltip unit testing', () => {
  const tooltipUnitDemo = `
    <li class="user-name">
      <span class="js-tooltip" data-truncate-characters="20">User-name-get-truncated-this-way-when-is-too-long-&Display-onToolTip
      </span>
      <span class="js-tooltip" data-truncate-px="160">User-name-get-truncated-this-way-when-is-too-long-&amp;Display-onToolTip@something.com
      </span>
      <span class="js-tooltip" data-truncate-px="160"></span>
      </span>
    </li>`

  beforeEach(() => {
    $('body').prepend(tooltipUnitDemo)
    textTruncate()
  })

  afterEach(() => {
    $('.user-name').remove()
  })

  context('truncate at character length', () => {
    it('should have at length of 20 characters given 20 as data attribute', () => {
      const target = $('.js-tooltip')[0]
      expect(target.textContent.length).to.equal(20)
    })

    it('should not have the same title attribute as the text content', () => {
      const target = $('.js-tooltip')[0]
      expect(target.getAttribute('title')).not.to.equal(target.textContent)
    })
  })

  context('truncate at px size', () => {
    it('should have at length of 160px given 160px as data attribute', () => {
      const target = $('.js-tooltip')[1]
      expect(target.offsetWidth).to.equal(160)
    })
  })

  context('other scenarios', () => {
    it('should not have title attribute if there is no textContent', () => {
      const target = $('.js-tooltip')[2]

      expect(target.innerText.length).to.equal(0)
      expect(target.getAttribute('title')).to.be.null
    })
  })
})
